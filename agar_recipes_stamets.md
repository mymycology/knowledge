# Agar recipes

## Potato, Dextrose, Agar (PDA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| potato water | 300 | gr | the broth from boiling potatoes in 2–3 liters of water for 1 hour |
| agar agar | 20 | gr | |
| dextrose | 10 | gr | |


## Malt Extract, Yeast Agar (MYA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| agar agar | 20 | gr | |
| barley malt sugar | 20 | gr | |
| yeast | 2 | gr | nutritional |
| peptone | 1 | gr | *optional*, soybean derived |

(The above medium is abbreviated as MYA. With the peptone, which is not critical for most of the species described in this book, this medium is designated MYPA.)


## Potato, Dextrose, Yeast Agar (PDYA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| potato water | 300 | gr | the broth from boiling potatoes in 2–3 liters of water for 1 hour |
| agar agar | 20 | gr | |
| dextrose | 10 | gr | |
| yeast | 2 | gr | nutritional |
| peptone | 1 | gr | *optional*, soybean derived |

(This medium is designated PDYA, or PDYPA if peptone is added. Note that only the broth from boiling the potatoes is used—the potatoes are discarded. The total volume of the media should equal 1 liter.)

## Oatmeal, Malt, Yeast Enriched Agar (OMYA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| instant oatmeal | 80 | gr | | 
| agar agar | 20 | gr | |
| malt sugar | 10 | gr | |
| yeast | 2 | gr | nutritional |

(This rich medium is called OMYA. The oatmeal does not have to be filtered out although some prefer to do so.)


## Dog Food Agar (DFA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| dry dog food | 20 | gr | |
| agar agar | 20 | gr | |

(This medium is called DFA.)

## Cornmeal, Yeast, Glucose Agar (CMYA)

| **substance** | **quantity** | **unit** | **comments** |
|:---|:--|:--|:--|
| water | 1000 | ml | |
| agar agar | 20 | gr | |
| cornmeal | 10 | gr | |
| malt/glucose | 5 | gr | |
| yeast | 1 | gr | nutritional |

(This medium is known as CMYA and is widely used by mycological laboratories for storing cultures and is not as nutritious as the other above-described formulas.)


## Sources:
* Stamets, Paul. Growing Gourmet and Medicinal Mushrooms (Kindle Locations 1947-1967). Potter/Ten Speed/Harmony/Rodale. Kindle Edition.

